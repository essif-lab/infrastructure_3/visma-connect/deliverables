# Terminology
- Holder: The entity that is holding the mandate. Holding the mandate provides the holder with the ability to act on behalf of the provider in a specified manner.

- Issuer: The entity that is actually creating the mandate. In our solution this is the Mandate Service. Creating a mandate is only done on initiative of the provider.

- Mandate: A mandate is a form of consent, The definition of a mandate is the authority granted by a proxy giver to act as its representative.

- Provider: The entity that is initiating the creation of a mandate. This entity is providing the mandate to the holder, making use of the Mandate Service.

- Roles: There are 4 roles specified in our solution. Provider, Holder, Verifier and Issuer.

- Verifier: The entity that needs to check the validity of a mandate.
