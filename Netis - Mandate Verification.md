Netis - Verifying mandates
==========================

# Table of contents
1. [Introduction](#Introduction)
2. [Solution](#Solution)
3. [Implementation](#Implementation)
    1. [Adding a policy to the mandate schema ](#AddingPolicy)
    2. [Verifying a mandate with a policy](#VerifyingMandateWithPolicy)


# Introduction <a name="Introduction"></a>
Netis has described a convenient way to define policies for verifiable credentials and provides a specific solution for verifiable mandates (https://docs.walt.id/v/ssikit/usage-examples/verifiable-credentials/delegation-and-mandates ).

The policy is based on the Open Policy Agent (https://www.openpolicyagent.org/), a unified toolset and framework for policies that can be used on every stack. It uses the language Rego, a high-level, declarative language that promotes safe, performant and fine-grained controls ( https://www.openpolicyagent.org/docs/latest/policy-language/ ).
The Open Policy Agent has a validate API to validate a mandate based on the given policy.

The SSI Mandate Service only validated whether a mandate adheres to a schema. Only the structure of the mandate was validated. With the introduction of OPA policies, the validation of mandates using a fine-grained declarative policy is possible.
The SSI Mandate Service is extended, so that it not only supports defining a schema, but also the creation of policy for a mandate, when creating a mandate.
Both schema and policy validation is done when verifying the mandate credential.


## References
| Description | Link |
| ------ | ------ |
| Open Policy Agent | https://www.openpolicyagent.org/ |
| Rego language | https://www.openpolicyagent.org/docs/latest/policy-language/ |
| Delegation and Mandates (SSI Kit)  | https://docs.walt.id/v/ssikit/usage-examples/verifiable-credentials/delegation-and-mandates |
| Open Policy Agent (SSI Kit) | https://docs.walt.id/v/ssikit/usage-examples/open-policy-agent |
| Netis ESSIF Lab project | https://gitlab.grnet.gr/essif-lab/infrastructure_3/netis/ADVM_project_summary |



# Solution <a name="Solution"></a>
The solution has 2 parts to enable the SSI Mandate Service to use policies:
1. It has to be possible to add OPA based policies when creating a schema for a mandate.
2. The added policy has to be used when verifying a mandate based on the schema to which the policy is added.

This means an extension of Step 1 : Creation of a new schema and Step 4: Requesting proof (Ask to verify) as mentioned in the [Functional Specification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/functional_specification.md) .

When creating a schema for a mandate, not only the attributes and the predicates on the attributes can be defined, but a policy can be created, validated and added as well.
When requesting for a proof, the verifier will not only check on the values of the attributes, but the mandate will be validated with the policy as well.

# Implementation <a name="Implementation"></a>

## Adding a policy to the mandate schema <a name="AddingPolicy"></a>
When creating a schema, a policy can be added to the schema. A policy is written in the Rego language. The code can be added to a text box. The code in that box can be syntax validated.

Below, an example of a schema with which can be checked whether a mandate to approve invoices is meeting certain criteria:
1. The verifier can check whether the mandate to approve invoices is applicable for invoices higher than a given amount (using the "Greater than" predicate when defining the proof request).
2. The policy checks whether the role of the authorized representative is a manager. This means that mandates can only be delegated to managers.

![Schema Manager containing a policy](Schema_Manager_Managersapproval.png)

So, the policy for checking whether the role in the mandate credential is "manager" in the Managersapproval schema is as follows:

![Example of a policy](Policy_Managersapproval.png)

When adding the policy to a schema, it has the following requirements:
- The package name needs to be the name and version of the schema, ':' and '.' charachters converted to '_'. For example: for title 'myschema' and version '1.0', this becomes: 'myschema_1_0'
- All attributes are given directly in the input object. Attributes can be used using 'input.<attribute name>. For example: an attribute 'username' can be used in a policy using 'input.username'
- The policy needs to have a rule names 'valid'. This rule is the main rule called and is used to determine the outcome of the policy.


## Verifying a mandate with a policy <a name="VerifyingMandateWithPolicy"></a>
Policy validation is done, when verifying the mandate.
When there is no policy defined for a schema, policy validation is skipped and the outcome is always valid.
When a policy exists for the schema, the 'valid' rule of the policy is evaluated and the result is used as validation result.

In this case, the verifier wants to check whether the mandate is given to a manager who can approve invoices higher than 3600 euro.
The verifier will create the following proof request for that:

![Proof Request Managersapproval](Proof_Request_Managersapproval.png)

The SSI Mandate Service will validate the mandate based on the policy.
