Architecture Description
=========================

You can use the Mandate service in two ways. 
1.  Use the supplied API-s (Rest API and WebSocket API) in your own system.
2.  Use the supplied web interface.

# Architectural Decisions
1.  To make the Mandate application scalable and resilient the application will be deployed in a Kubernetes cluster. 
2.  Where possible we will use components / libraries of third parties.

![Architecture](Architecture_SSI_Mandate.png)

# Components

The Mandate application itself consist also of a number of module.
1.  Mandate service: this is the microservice that contains the business logic. The service is triggered by either an api-call, a call to the webhook from ACA-PY or from timed events.
2.  Mandate Interface: this is the microservice that exposes the Rest API and WebSocket API that are called from client programs. Here authentication/authorisation and validations takes place.  
The Mandate Interface exposes two API-s: [Rest API](OpenApi.md) and [WebSocket](AsyncApi.md)
3.  Mandate Web Hook: this microservice is called by ACA-PY to notify the Mandate Application of events that happen on the SSI platform. For example when a connection request is accepted, the Indy platform calls the agent (ACA-PY) and this in turn calls the webhook.
4.	Mandate Data: The mandate application needs to store data. Most required data is stored in the Wallet. But for some purposes extra data needs to be stored. Eg for revocation.
5.  Web Admin: this react application is used to perform administrative taks on the application.
6.  Mandate Web: this react application can be used to interact with the Mandate application though a UI. This application offers the ability to create new mandate schema's, create new mandates, revoke mandates and verify existing mandates.

To interact with the SSI platform we use a number of third party components.
1.  [ACA-PY](https://github.com/hyperledger/aries-cloudagent-python): this is the agent that handles the communication between an INDY platform (eg Sovrin) and the Mandate application. The Mandate application calls the rest-api of ACA-PY to send messages to the platform and the ACA-PY calls the Mandate Web Hook if interesting events arrive from the platform. The ACA-PY is also the agent of the Mandate application that communicates with other agents (eg on the phone of a holder).
2.  [Tailserver](https://github.com/bcgov/indy-tails-server): this server handles the distribution of the tailfiles to the INDY platform. Tailsfiles are the basis for the revocation of credentials.
3.  Wallet: The ACA-PY needs to store information about created artifacts (eg credentials, schema's, connections). This is done in a wallet. ACA-PY supports two kind of wallets. Indy-based wallets and wallets stored in Postgress. To make de Mandate application scalable using kubernetes, Postgress has to be used.

# Interfacing

The Mandate application has three points were it interfaces with the outside world.
1.  The Mandate Interface component can be called by clients to initiate the creation of schema's and mandates, to revoke issued mandates and to verify mandates.
2.  The ACA-PY component is the agent that interacts with the SSI-platform and other agents. It sends information to the platform and is notified of relevent events.
3.  The Tails server is the component that sends the [tails files](https://hyperledger-indy.readthedocs.io/projects/sdk/en/latest/docs/concepts/revocation/cred-revocation.html#background-tails-files) (used for revocation) to the SSI-platform. 


# Function of the components
The function of the components is further explained in the [technical flow](technical_flow.md)