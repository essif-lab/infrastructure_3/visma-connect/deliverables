OpenApi Description
===================

This is the description of the rest api that can be used to interact with the mandate application.

See [interface specs](interface_specification.md) for more information.

# OpenApi description of the mandate-interface

```
{
    "openapi": "3.0.1",
    "info": {
        "title": "OpenAPI definition",
        "version": "v0"
    },
    "servers": [
        {
            "url": "http://localhost:8443",
            "description": "Generated server url"
        }
    ],
    "paths": {
        "/schemas": {
            "get": {
                "tags": [
                    "schema-api-controller"
                ],
                "summary": "Get a list of available schema's",
                "operationId": "getAvailableSchemas",
                "parameters": [
                    {
                        "name": "onlyUserSchemas",
                        "in": "query",
                        "description": "Flag to decide if response should include all schemas or just user schemas",
                        "required": false,
                        "schema": {
                            "type": "boolean",
                            "default": false
                        },
                        "example": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "The list with schemas is returned.",
                        "content": {
                            "*/*": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/components/schemas/AvailableSchemasResponse"
                                    }
                                },
                                "example": [
                                    {
                                        "schemaId": "WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0",
                                        "title": "Schema Name",
                                        "name": "schema_name"
                                    },
                                    {
                                        "schemaId": "WgWxqztrNooG92RXvxSTWv:2:another_schema_name:2.0",
                                        "title": "Another Schema Name",
                                        "name": "another_schema_name"
                                    }
                                ]
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error"
                    },
                    "204": {
                        "description": "No schemas are available",
                        "content": {
                            "*/*": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/components/schemas/AvailableSchemasResponse"
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "post": {
                "tags": [
                    "schema-api-controller"
                ],
                "summary": "Add schema",
                "operationId": "addSchema",
                "requestBody": {
                    "description": "New schema details",
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/AddSchemaRequest"
                            },
                            "example": {
                                "name": "schema-name",
                                "title": "Schema Name",
                                "version": "1.0",
                                "attributes": [
                                    {
                                        "name": "attribute-name",
                                        "title": "Attribute Name",
                                        "type": "number",
                                        "hasPredicates": true
                                    }
                                ],
                                "predicates": [
                                    {
                                        "name": "attribute-name",
                                        "type": "number",
                                        "conditions": [
                                            {
                                                "title": "Condition Title",
                                                "value": ">"
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    },
                    "required": true
                },
                "responses": {
                    "500": {
                        "description": "Internal Server Error"
                    },
                    "200": {
                        "description": "Schema is already added",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "string"
                                },
                                "examples": {
                                    "Status of existing schema": {
                                        "summary": "Status of existing schema",
                                        "description": "Status of existing schema",
                                        "value": {
                                            "title": "Schema Title",
                                            "status": "already_exists"
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "201": {
                        "description": "Schema has been added",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "type": "string"
                                },
                                "examples": {
                                    "Status of schema addition": {
                                        "summary": "Status of schema addition",
                                        "description": "Status of schema addition",
                                        "value": {
                                            "title": "Schema Title",
                                            "status": "ok"
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Invalid request"
                    }
                }
            }
        },
        "/proofs": {
            "post": {
                "tags": [
                    "present-proof-controller"
                ],
                "summary": "Create a new Request for Proof",
                "operationId": "createNewProofRequest",
                "parameters": [
                    {
                        "name": "schemaName",
                        "in": "query",
                        "description": "The url-encoded id of the mandate-schema which needs to be proofed",
                        "required": true,
                        "schema": {
                            "type": "string"
                        },
                        "example": "mandate-signing-basic%3A1.0"
                    },
                    {
                        "name": "comment",
                        "in": "query",
                        "description": "Comment to be displayed in the proof request",
                        "required": false,
                        "schema": {
                            "type": "string"
                        },
                        "example": "Proof required"
                    }
                ],
                "requestBody": {
                    "description": "The actual mandate request (must match the layout of the mandate-schema)",
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "string"
                            },
                            "example": [
                                {
                                    "credential": "authorization"
                                },
                                {
                                    "credential": "spending-limit",
                                    "predicate": ">=",
                                    "condition": "10"
                                }
                            ]
                        }
                    },
                    "required": true
                },
                "responses": {
                    "500": {
                        "description": "Internal Server Error",
                        "content": {
                            "*/*": {
                                "schema": {
                                    "$ref": "#/components/schemas/ExceptionResponse"
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Invalid request",
                        "content": {}
                    },
                    "201": {
                        "description": "A new request for proof is created.",
                        "content": {
                            "text/plain": {
                                "schema": {
                                    "type": "string"
                                },
                                "examples": {
                                    "Base64 encoded proof request to be used in displaying a QR-Code.": {
                                        "summary": "Base64 encoded proof request",
                                        "description": "Base64 encoded proof request to be used in displaying a QR-Code.",
                                        "value": "aHR0cDovL2FyaWVzLWNsb3VkYWdlbnQtcnVubmVyOjgwMjA/Y19pPWV5SkFkSGx3WlNJNklDSmthV1E2YzI5Mk9rSjZRMkp6VGxsb1RYSnFTR2x4V2tSVVZVRlRTR2M3YzNCbFl5OWpiMjV1WldOMGFXOXVjeTh4TGpBdmFXNTJhWFJoZEdsdmJpSXNJQ0pBYVdRaU9pQWlOR0kxTXpFM05qWXROV1ExTmkwMFpUYzRMV0ZsWW1JdE1UVmpOemN4WXpJeFltWm1JaXdnSW5KbFkybHdhV1Z1ZEV0bGVYTWlPaUJiSWtjeFNtaGFja1paWldad1VuUTFlakpxWlVwMFRIQjVjV1pyWldJeVVtRnBkRFZyUldkRlVGVlJOV1JYSWwwc0lDSnpaWEoyYVdObFJXNWtjRzlwYm5RaU9pQWlhSFIwY0RvdkwyRnlhV1Z6TFdOc2IzVmtZV2RsYm5RdGNuVnVibVZ5T2pnd01qQWlMQ0FpYkdGaVpXd2lPaUFpYkc5allXeEJjbWxsYzBOc2IzVmtRV2RsYm5RaWZRPT0="
                                    }
                                }
                            }
                        }
                    }
                },
                "deprecated": true
            }
        },
        "/mandates": {
            "post": {
                "tags": [
                    "mandate-api-controller"
                ],
                "summary": "Submit a new request for a mandate",
                "operationId": "createNewMandateRequest",
                "parameters": [
                    {
                        "name": "schemaName",
                        "in": "query",
                        "description": "The url-encoded id of the mandate-schema",
                        "required": true,
                        "schema": {
                            "type": "string"
                        },
                        "example": "mandate-signing-basic%3A1.0"
                    },
                    {
                        "name": "alias",
                        "in": "query",
                        "description": "Alias that can be used to identify the connection that will be created",
                        "required": false,
                        "schema": {
                            "type": "string"
                        },
                        "example": "Alice"
                    }
                ],
                "requestBody": {
                    "description": "The actual mandate request (must match the layout of the mandate-schema)",
                    "content": {
                        "application/json": {
                            "schema": {
                                "type": "string"
                            },
                            "example": {
                                "authorization": "Ralph",
                                "spending-limit": "10"
                            }
                        }
                    },
                    "required": true
                },
                "responses": {
                    "500": {
                        "description": "Internal Server Error",
                        "content": {
                            "*/*": {
                                "schema": {
                                    "$ref": "#/components/schemas/ExceptionResponse"
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Invalid request",
                        "content": {}
                    },
                    "201": {
                        "description": "A new request for the mandate is created.",
                        "content": {
                            "text/plain": {
                                "schema": {
                                    "type": "string"
                                },
                                "examples": {
                                    "Base64 encoded invitation to be used in displaying a QR-Code.": {
                                        "summary": "Base64 encoded invitation",
                                        "description": "Base64 encoded invitation to be used in displaying a QR-Code.",
                                        "value": "aHR0cDovL2FyaWVzLWNsb3VkYWdlbnQtcnVubmVyOjgwMjA/Y19pPWV5SkFkSGx3WlNJNklDSmthV1E2YzI5Mk9rSjZRMkp6VGxsb1RYSnFTR2x4V2tSVVZVRlRTR2M3YzNCbFl5OWpiMjV1WldOMGFXOXVjeTh4TGpBdmFXNTJhWFJoZEdsdmJpSXNJQ0pBYVdRaU9pQWlOR0kxTXpFM05qWXROV1ExTmkwMFpUYzRMV0ZsWW1JdE1UVmpOemN4WXpJeFltWm1JaXdnSW5KbFkybHdhV1Z1ZEV0bGVYTWlPaUJiSWtjeFNtaGFja1paWldad1VuUTFlakpxWlVwMFRIQjVjV1pyWldJeVVtRnBkRFZyUldkRlVGVlJOV1JYSWwwc0lDSnpaWEoyYVdObFJXNWtjRzlwYm5RaU9pQWlhSFIwY0RvdkwyRnlhV1Z6TFdOc2IzVmtZV2RsYm5RdGNuVnVibVZ5T2pnd01qQWlMQ0FpYkdGaVpXd2lPaUFpYkc5allXeEJjbWxsYzBOc2IzVmtRV2RsYm5RaWZRPT0="
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/mandates/revoke": {
            "post": {
                "tags": [
                    "mandate-api-controller"
                ],
                "summary": "Revoke credential",
                "operationId": "revokeCredential",
                "requestBody": {
                    "description": "The issued mandate request",
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/IssuedMandateResponse"
                            },
                            "example": {
                                "schemaName": "mandate-signing-basic:1.0",
                                "connectionId": "446a0164-e59d-443f-9287-23554111f7a9",
                                "values": [
                                    {
                                        "credName": "authorization",
                                        "credValue": "Books"
                                    }
                                ],
                                "credRevId": "1",
                                "revRegId": "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
                            }
                        }
                    },
                    "required": true
                },
                "responses": {
                    "500": {
                        "description": "Internal Server Error",
                        "content": {}
                    },
                    "400": {
                        "description": "Invalid request",
                        "content": {}
                    },
                    "200": {
                        "description": "Credential has been revoked",
                        "content": {
                            "text/plain": {
                                "schema": {
                                    "type": "string"
                                },
                                "examples": {
                                    "Status of revocation": {
                                        "summary": "Status of revocation",
                                        "description": "Status of revocation",
                                        "value": "success"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/internal/proofs/{uuid}": {
            "patch": {
                "tags": [
                    "internal-proof-controller"
                ],
                "summary": "Receive an updated status for a given proof request",
                "operationId": "updateProofRequest",
                "parameters": [
                    {
                        "name": "uuid",
                        "in": "path",
                        "description": "user-id of the proofrequest",
                        "required": true,
                        "schema": {
                            "type": "string"
                        },
                        "example": 1234
                    }
                ],
                "requestBody": {
                    "description": "The information to use in the update",
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/ProofState"
                            },
                            "example": {
                                "id": "abc-def",
                                "state": "succeeded"
                            }
                        }
                    },
                    "required": true
                },
                "responses": {
                    "200": {
                        "description": "Proof request has been updated",
                        "content": {
                            "text/plain": {
                                "schema": {
                                    "type": "string"
                                },
                                "examples": {
                                    "ok": {
                                        "summary": "ok message",
                                        "description": "ok"
                                    }
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Invalid request"
                    }
                }
            }
        },
        "/schemas/{schemaId}": {
            "get": {
                "tags": [
                    "schema-api-controller"
                ],
                "summary": "Get schema definition",
                "operationId": "getSchemaDefinition",
                "parameters": [
                    {
                        "name": "schemaId",
                        "in": "path",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Schemas definition is returned.",
                        "content": {
                            "*/*": {
                                "schema": {
                                    "$ref": "#/components/schemas/SchemaDefinitionResponse"
                                },
                                "example": {
                                    "schemaId": "55MgvkQDB815vtbFky4kEt:2:mandate-schema:1.1",
                                    "name": "mandate-schema",
                                    "title": "Mandate Schema",
                                    "version": "1.1",
                                    "attributes": [
                                        {
                                            "name": "authorization",
                                            "title": "Authorization",
                                            "type": "text"
                                        }
                                    ],
                                    "predicates": [
                                        {
                                            "name": "spending-limit",
                                            "type": "number",
                                            "condition": [
                                                {
                                                    "title": "Greater than",
                                                    "value": ">"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error"
                    },
                    "404": {
                        "description": "Schema definition not found",
                        "content": {
                            "*/*": {
                                "schema": {
                                    "$ref": "#/components/schemas/SchemaDefinitionResponse"
                                }
                            }
                        }
                    }
                }
            }
        },
        "/proofs/{uuid}": {
            "get": {
                "tags": [
                    "present-proof-controller"
                ],
                "summary": "Retrieve a stored Request for Proof",
                "operationId": "retrievePresentationRequest",
                "parameters": [
                    {
                        "name": "uuid",
                        "in": "path",
                        "required": true,
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "500": {
                        "description": "Internal server error",
                        "content": {}
                    },
                    "400": {
                        "description": "Invalid request",
                        "content": {}
                    },
                    "301": {
                        "description": "A proof request if found and returned.",
                        "content": {
                            "text/plain": {
                                "schema": {
                                    "type": "string"
                                },
                                "examples": {
                                    "url with the request": {
                                        "summary": "UrlEncode Base64 encoded proof request",
                                        "description": "url with the request",
                                        "value": "\"http://localhost:8443?d_m=aHR0cDovL2FyaWVzLWNsb3VkYWdlbnQtcnVubmVyOjgwMjA/Y19pPWV5SkFkSGx3WlNJNklDSmthV1E2YzI5Mk9rSjZRMkp6VGxsb1RYSnFTR2x4V2tSVVZVRlRTR2M3YzNCbFl5OWpiMjV1WldOMGFXOXVjeTh4TGpBdmFXNTJhWFJoZEdsdmJpSXNJQ0pBYVdRaU9pQWlOR0kxTXpFM05qWXROV1ExTmkwMFpUYzRMV0ZsWW1JdE1UVmpOemN4WXpJeFltWm1JaXdnSW5KbFkybHdhV1Z1ZEV0bGVYTWlPaUJiSWtjeFNtaGFja1paWldad1VuUTFlakpxWlVwMFRIQjVjV1pyWldJeVVtRnBkRFZyUldkRlVGVlJOV1JYSWwwc0lDSnpaWEoyYVdObFJXNWtjRzlwYm5RaU9pQWlhSFIwY0RvdkwyRnlhV1Z6TFdOc2IzVmtZV2RsYm5RdGNuVnVibVZ5T2pnd01qQWlMQ0FpYkdGaVpXd2lPaUFpYkc5allXeEJjbWxsYzBOc2IzVmtRV2RsYm5RaWZRPT0=&orig=http://localhost:8443/proofs/abcd"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        "/mandates/issued": {
            "get": {
                "tags": [
                    "mandate-api-controller"
                ],
                "summary": "Retrieve list of issued mandates",
                "operationId": "getIssuedMandates",
                "responses": {
                    "500": {
                        "description": "Internal Server Error",
                        "content": {}
                    },
                    "400": {
                        "description": "Bad Request",
                        "content": {
                            "*/*": {
                                "schema": {
                                    "$ref": "#/components/schemas/ExceptionResponse"
                                }
                            }
                        }
                    },
                    "200": {
                        "description": "List of IssuedMandateResponse returned.",
                        "content": {
                            "*/*": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/components/schemas/IssuedMandateResponse"
                                    }
                                },
                                "examples": {
                                    "IssuedMandateResponse": {
                                        "summary": "IssuedMandateResponse",
                                        "description": "IssuedMandateResponse",
                                        "value": [
                                            {
                                                "schemaName": "mandate-signing-basic:1.0",
                                                "connectionId": "446a0164-e59d-443f-9287-23554111f7a9",
                                                "values": [
                                                    {
                                                        "credName": "authorization",
                                                        "credValue": "Books"
                                                    }
                                                ],
                                                "credRevId": "1",
                                                "revRegId": "WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0"
                                            }
                                        ]
                                    }
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "No issued credentials are available",
                        "content": {
                            "*/*": {
                                "schema": {
                                    "type": "array",
                                    "items": {
                                        "$ref": "#/components/schemas/IssuedMandateResponse"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "components": {
        "schemas": {
            "ExceptionResponse": {
                "type": "object",
                "properties": {
                    "message": {
                        "type": "string"
                    }
                }
            },
            "AddSchemaRequest": {
                "required": [
                    "attributes",
                    "name",
                    "title",
                    "version"
                ],
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "title": {
                        "type": "string"
                    },
                    "version": {
                        "pattern": "^([0-9]+\\.[0-9])$",
                        "type": "string"
                    },
                    "attributes": {
                        "maxItems": 2147483647,
                        "minItems": 1,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/Attribute"
                        }
                    },
                    "predicates": {
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/Predicate"
                        }
                    }
                }
            },
            "Attribute": {
                "required": [
                    "name",
                    "title",
                    "type"
                ],
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "title": {
                        "type": "string"
                    },
                    "type": {
                        "type": "string"
                    }
                }
            },
            "Condition": {
                "required": [
                    "title",
                    "value"
                ],
                "type": "object",
                "properties": {
                    "title": {
                        "type": "string"
                    },
                    "value": {
                        "type": "string",
                        "enum": [
                            "<",
                            "<=",
                            ">=",
                            ">"
                        ]
                    }
                }
            },
            "Predicate": {
                "required": [
                    "condition",
                    "name",
                    "type"
                ],
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "type": {
                        "type": "string"
                    },
                    "condition": {
                        "maxItems": 2147483647,
                        "minItems": 1,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/Condition"
                        }
                    }
                }
            },
            "IssuedMandateResponse": {
                "type": "object",
                "properties": {
                    "schemaName": {
                        "type": "string"
                    },
                    "connectionId": {
                        "type": "string"
                    },
                    "values": {
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/Value"
                        }
                    },
                    "credRevId": {
                        "type": "string"
                    },
                    "revRegId": {
                        "type": "string"
                    }
                }
            },
            "Value": {
                "type": "object",
                "properties": {
                    "credName": {
                        "type": "string"
                    },
                    "credValue": {
                        "type": "string"
                    }
                }
            },
            "ProofState": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "string"
                    },
                    "state": {
                        "type": "string"
                    }
                }
            },
            "AvailableSchemasResponse": {
                "type": "object",
                "properties": {
                    "schemaId": {
                        "type": "string"
                    },
                    "title": {
                        "type": "string"
                    },
                    "name": {
                        "type": "string"
                    }
                }
            },
            "SchemaDefinitionResponse": {
                "required": [
                    "attributes",
                    "name",
                    "predicates",
                    "schemaId",
                    "title",
                    "version"
                ],
                "type": "object",
                "properties": {
                    "schemaId": {
                        "type": "string"
                    },
                    "name": {
                        "type": "string"
                    },
                    "title": {
                        "type": "string"
                    },
                    "version": {
                        "type": "string"
                    },
                    "attributes": {
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/Attribute"
                        }
                    },
                    "predicates": {
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/Predicate"
                        }
                    }
                }
            }
        }
    }
}
```

