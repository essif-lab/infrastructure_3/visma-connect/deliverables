Functional specification
========================

# Functional Description
With the Mandate application you will be able to authorize a person to perform some tasks for you. You do this by giving them an SSI credential that states what that person can do. 
Because different mandate use-cases need different information you can also create a schema that is tailored to your use case. 

The Mandate application contains two of different components. A front-end and a back-end.

With the back-end component you can perform all actions required to use SSI mandates. You can create schema(s) you required. That schema can than be used to issue, validate and revoke mandates. 
You do this by calling the mandate-api. 


With the front-end component you can perform all actions required to issue and validate mandates like you can with the mandate-api of the back-end component, but by using a simple web-application.
This web application is mainly used as an example of how you can use the mandate back-end within your own application. But is is also possible to use this front-end to create mandates.

The [interface specification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/interface_specification.md) describes how you can use the Mandate Service using an API. The [Architecture](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Architecture.md) describes what components are involved in the solution.

# Example Process and Flow
### Introduction:
- There is a need for an authorization which can be proven to a third party. Due to the special needs of this authorization, a new schema needs to be created.
- In every step a link is made to two fictional examples.  
- In the first example the CBR (or DMV in the USA) is going to create its first driver's license.  
- In the second example a company charged with the security of a construction site (Heras) wants to grant access to the main contractor (BAM) and provides them with the ability to grant access to subcontractors.  


### Step 1: Creation of a new schema.  
- This schema holds all the conditions and restrictions that an authorization must meet.  
- The creation of a schema can be done by any party within the ecosystem.  
- Actor: Any role, but most likely the Provider.  
- Example 1: This is done by the CBR, but only once. This schema can be reused for every new driver's license, but needs to be available before creating a new one.  
- Example 2: Heras creates two schema which describe all the elements needed for the visioned mandates. One schema can be used to only allow access to a construction site. The second schema will allow the holder access, but in addition is the holder allowed to provide another party with a mandate to allow access. These schemas can be reused on several construction sites.

### Step 2: Creation of a mandate request.
- The owner of the information uses the schema created in the previous step and provides all the needed information.  
- Once completed, a mandate request is send to the intended holder.  
- Actor: The provider.  
- Example 1: The CBR (Provider) initiates this step. The mandate service (issuer) creates the mandate request. The driver receives this request.  
- Example 2: Heras (Provider) initiates this step. The mandate service (issuer) creates the mandate request. BAM as main contractor (holder) will receive this request. 


### Step 3: Accepting the mandate request (creation of the mandate)
- The intended holder receives the mandate request. They can accept or reject it. If accepted, the mandate is created.   
- Actor: The holder.  
- Example 1: The driver checks if the information is correct, if this is the case, they accept the driver's license in their wallet.  
- Example 2: Bam will check it the information is correct and accept the mandate. The mandate is stored in their wallet. From now on they have access to the building site and, since that is stated in their mandate, the possibility to grant other contractors access as well.
If Bam creates mandates for their subcontractors, it will act as provider and as holder. They create a new mandate (provider), but are only able to do so because their mandate enables them (holder).  


### Step 4: Requesting proof (Ask to verify)
- A third party (the verifier) wants to check if the holder of the mandate is allowed to perform the specified action.
- The verifier creates a request, based on the schema (created in step 1). The verifier chooses the appropriate values,
and presents the request to the holder.
- Actor: The verifier.
- Example 1: The driver is stopped by the police. The police wants to check if the driver is in possession of a valid driver's license.
- Example 2: A subcontractor with a valid mandate wants to enter the construction site. They face a turnstile gate with a NFC pad or QR code. Scanning either one with their wallet, will result in receiving the verification request. 
The subcontractor is the holder, the turnstile gate acts as the verifier.

### Step 5: Providing proof (Verify)
- The holder receives the request for proof. To verify, they need to accept the requested. If the holder accepts, 
the information is checked with the proof on the blockchain and the verifier receives a conformation.
- Actor: The holder.
- Example 1: The driver, which is still stopped by the police, accepts the validation requests. The police is now informed that this driver is allowed to drive.
- Example 2: The subcontractor wants to enter the construction site, so they accept the request. The turnstile gate receives the proof and will allow the subcontractor access.


### Step 6: Withdraw of the mandate (Revoke)
- If the creator of the mandate wants to withdraw the mandate, they can revoke it.
- If done so, the mandate will get marked as invalid on the blockchain.
- Actor: The provider.
- Example 1: After a DUI, the driver's license needs to get revoked. The CBS revokes the driver's license.
- Example 2: The contract of a subcontractor has ended. Bam revokes the mandate of the subcontractor. Bam is the provider, the subcontractor the holder.


### Step 7: Requesting proof (Ask to verify after revocation)
- This step is the same as step 4, since the verifier is unaware if the mandate is revoked.
- Actor: The verifier.
- Example 1: The stubborn driver is again stopped by the police. The police wants to check if the driver is in possession of a valid driver's license.
- Example 2: The subcontractor wants to enter the construction site. Their motive is unclear to us. they will scan the qr code, provided by the turnstile gate. 

### Step 8: Providing proof(Verify after revocation)
- The holder might be unaware of the revocation. They present the proof as in step 5.
- The verifier will receive a response which marks the presented proof is insufficient.
- Actor: The holder.
- Example 1: The driver's license is no longer valid. The police will be informed about this. The driver can be detained.
- Example 2: The subcontractor is still the holder of the mandate and approves the request hoping to gain access. The turnstile gate will receive the proof, but will also check the revocation list.  The gate will not open, since the mandate is revoked.
