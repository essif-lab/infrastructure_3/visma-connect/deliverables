Interface specification
=======================

You can use the Mandate service in two ways. 
1.  Use the supplied web application (Mandate Web).
1.  Use the supplied API-s in your own system directly.

In this document we will describe how you can use the API-s.
For each step that is mentioned in the [functional specifications](functional_specification.md) you will find an explanation how to use the API.  
Technical details for the Rest Api can be found [here](OpenApi.md)  
Technical details for the WebSocket Api can be found [here](AsyncApi.md)

## Create Schema
-	You can check which schema's are already available with the Rest Api: GET /schemas.
-	If the schema you require is not available, you can create a new schema with the Rest Api: POST /schemas
-	Once you created a schema (or found an existing one) you can retrieve the details for that schema with the Rest Api: GET /schemas/{schemaId}.
Note that in the mandate service a schema contains both information about the mandate (the ssi credential) and the way of verification.

## Create mandate request
-	You can request a new Mandate by using the Rest Api: Post /mandates. 

## Accepting a mandate request
This process is handled outside of the api-s. The application and holder of the mandate use the INDY platform for that. Communication is done between the agents of the mandate application and the agent of the holder.

## Requesting proof
The verification of a mandate (requesting proof of the madate) is a three step process.
1.  Request the proof from a holder. This is done by invoking the WebSocket Api: /proof/request  
2.  The holder will sent the proof to the agent of mandate application through the holders agent
3.  The mandate application will send the result of the received prove back to the requester usering the WebSocket: /user/present-proof/response

## Withdraw of the mandate
-	You can retrieve a list of all the issued mandates with the Rest Api: GET /mandates/issued
-	You can than revoke a mandate with the Rest Api: POST /mandates/revoke.