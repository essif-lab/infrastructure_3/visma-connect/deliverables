AsyncApi Description
====================

This is the description of the WebSocket api that can be used to interact with the mandate application.

See [interface specs](interface_specification.md) for more information.

# AsyncApi description of the mandate-interface

```
asyncapi: '2.2.0'
info:
  title: Mandate websocket for proof
  version: 0.0.1
  description: This service is used to request proof (verification) and report on the status.
servers:
  public:
    url: http://localhost/proof/request
    protocol: ws
    description: |
      Public server available without authorization.
      Once the socket is open you can subscribe to a public channel by sending a subscribe request message.
channels:
  /user/present-proof/response:
    publish:
      message:
        $ref: '#/components/messages/ProofRequest'
      description: 
        Request a new proof and wait for response to be published
    subscribe:
      message:
        $ref: '#/components/messages/ProofResponse'
      description: 
        response to the requested proof
components:
  messages:
    ProofRequest:
      payload:
        type: object
        properties:
          $schema:
            type: string
          type:
            type: string
          properties:
            type: object
            properties:
              schemaName:
                type: object
                properties:
                  type:
                    type: string
                  description:
                    type: string
              comment:
                type: object
                properties:
                  type:
                    type: string
                  description:
                    type: string
              jsonRequest:
                type: object
                properties:
                  type:
                    type: string
                  description:
                    type: string
          required:
            type: array
            items:
              type: string
    ProofResponse:
      payload:
        type: object
        properties:
          $schema:
            type: string
          type:
            type: string
          properties:
            type: object
            properties:
              state:
                type: object
                properties:
                  type:
                    type: string
                  enum:
                    type: array
                    items:
                      type: string
                  description:
                    type: string
              response:
                type: object
                properties:
                  type:
                    type: string
                  description:
                    type: string
          required:
            type: array
            items:
              type: string
```