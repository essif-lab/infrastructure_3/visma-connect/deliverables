Feasibility Report - SSI Mandate Service
========================================

# Table of contents
1. [Background](#Background)
2. [Goals versus Achievements](#GoalsVersusAchievements)
3. [Deviations and Takeaways](#DeviationsAndTakeaways)
4. [Usability](#Usability)
5. [Deliverables](#Deliverables)
    1. [Specifications and Documentation](#SpecificationsDocumentation)
    2. [Source code](#SourceCode)
6. [Demo Application](#DemoApplication)
7. [Publications](#Publications)
8. [Final Words](#FinalWords)



## Background <a name="Background"></a>
As part of the eSSIF-Lab grant, Visma Connect has created the SSI Mandate Service. The main purpose of this service is to make it easy to define mandates by configuring schemas and policies, and to guide the participants in issuing and verifying mandates based on those schemas and policies.The SSI Mandate Service should really support all type of mandates and authorizations. For that, we provide a simple Mandate API to use the mandate service to issue mandate credentials and we have a web based management interface with which the structure of a mandate can be defined and with which mandate proposals can be created and verified. We support all the different parties participating in a mandate request flow.
The SSI Mandate Service is running in a demo environment, available for everyone. All the code of the SSI Mandate Service is made open source and the service is extended with functionality from the other ESSIF-Lab parties: Netis and Bloqzone. 


## Goals versus Achievements <a name="GoalsVersusAchievements"></a>
The table below describes how the goals as defined in [Envisioned interoperability](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/envisioned_interoperability_with_others.md) are achieved.


| Goal | Achievement against set goal |
| ------ | ------ |
| Bloqzone - SSIComms - Identifying parties | We have added identication of parties using SSIComms (see [Bloqzone - SSIComms](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Bloqzone%20-%20SSIComms.md)). This is done by supplying the SIP ID attributes (SIP: protocol used in the SSIComms) in the mandate and making it possible to retrieve the mandate credential from the wallet using an invitation URL. The whole flow with SSIComms works as designed. The only thing we should add is a check on the SIP ID of the mandate provider (the issuer of the mandate). For now, any ID can be filled in when creating the mandate proposal. |
| TNO - EASSI - Improving Interoperability | We have not integrated the EASSI gateway of TNO in our solution to support more wallets. The SSI Mandate Service is already interoperable with Trinsic, Esatus, and Lissi, because they are using the same Aries Interop Profile [AIP](https://aries-interop.info/aries-interop-intro.html). By using EASSI we could support IRMA, but IRMA does not support revocation and revocation is an essential part of our solution. Next to that, we focussed on integrating with the app of Bloqzone which means you are locked into a specific wallet based on Animo anyway. However, using the EASSI Gateway is still an option, for instance to support Jolocom. |
|Netis - SSI Kit - Adding policy validation | Netis has described a convenient way to define policies for verifiable credentials and verifiable mandates. Those policies are based on the Open Policy Agent, a unified framework that can be used everywhere. It uses the language Rego, a high-level, declarative language that enables fine-grained controls. The SSI Mandate Service only validated whether a mandate adheres to a schema (the structure of the mandate). With the introduction of OPA policies, the validation of mandates using a fine-grained declarative policy is possible. The SSI Mandate Service is extended, so that it not only supports defining schemas, but that it supports the creation of policies for mandates as well. Both schema and policy validation is done when verifying the mandate credential. This is all explained in [Netis - Mandate Verification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Netis%20-%20Mandate%20Verification.md) |
|Required standards and agreements| Standards and agreements are important to make the SSI Mandate Service profitable and useful in an ecosystem Netis has done research on standards for Authority Delegation (AD) and Verifiable Mandates (VM) which will be available in [this research paper](https://gitlab.grnet.gr/essif-lab/infrastructure_3/netis/deliverables/-/blob/master/Delegation___Mandates_ESSIFLabs__Infrastructure_Call__-_RESEARCH_PAPERS.pdf). Really important is standardisation on the attributes of the credentials. There are initiatives with credential catalogues that register and advertise the information about credential types, but standardisation on attribute-level (what is the attribute for the date of birth, name, address and so on) would be helpful as well. Another challenge is the lack of standardisation from EBSI. Documentation of Netis and others helps to show what an EBSI/ESSIF compliant W3C Verifiable Credential / Mandate is, but more is needed to integrate with EBSI. Gaia-X is an interesting data infrastructure to add SSI-based identity to as well. The EIDAS v2 regulation might help to enable crossborder services using SSI. 

## Deviations and Takeaways <a name="DeviationsAndTakeaways"></a>
Although we have not deviated from the original goals, we have underestimated the challenges with EBSI and Sovrin Guardianship Working Group regarding standardisation and agreements. The Sovrin Guardianship Working Group was not very active and did not give much guidance on standardisation. Next to that, it did not become clear what is needed to make our SSI Mandate Service part of EBSI.

## Usability <a name="Usability"></a> 
The SSI Mandate Service is built with the mindset that decentralized mandating really take off if it can be applied by consultants and developers that are not familiair with SSI or blockchain. This means that the management interface should be easy to use and that the API should not require developers to know about HyperLedger or getting to know the underlying technology, but should instead offer a documented API and some clear use cases. 
Although a clear API helps a lot, developers need well written documentation too. Therefore we strive to create clear documentation that goes beyond just explaining the API. This also means that we want to create more tutorials for common practices like how to define different type of mandates for different use cases, how to use the mandate API and how the service can be used for the different types of mandate request flows.
Next to documenting the API, the source code itself is documented as well, so that developers that have SSI knowledge and really wants to change or extend the existing code are able to do that as well.

## Deliverables <a name="Deliverables"></a>
We have finished writing the [documentation]( https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Feasibility%20Report%20SSI%20Mandate%20Service.md#SpecificationsDocumentation) for the service. We only have to finish some minor details related to the integration with SSIComms of Bloqzone.
The [source code](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/SSIM) is delivered. All the code is documented as well.


### Specifications and Documentation <a name="SpecificationsDocumentation"></a>

- [functional specification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/functional_specification.md) - describes the functional mandate request flow
- [interface specification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/interface_specification.md) - describes how you can use the Mandate Service using an API.
- [envisioned interoperability with others](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/envisioned_interoperability_with_others.md) - describes with what parties we want to cooperate to achieve the abovementioned goals. 
- [architecture](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Architecture.md) - describes what components are involved in the solution.
- [API Specification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/OpenApi.md) - describes the mandate API itself
- [Bloqzone - SSIComms](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Bloqzone%20-%20SSIComms.md) - describes how SSIComms of Bloqzone is used to add idenfitication of parties in a mandate request flow.
- [Netis - Mandate Verification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Netis%20-%20Mandate%20Verification.md) - describes how policies can be added to a mandate schema and how they are used to verify a mandate.
- [Terminology](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Terminology.md) - clarifies the terms we use.
- Feasibility report (this document)


### Source code <a name="SourceCode"></a>
- [Source Code](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/SSIM)
    - All the source code is licensed under Apache 2.0.
- All the work that has been done is merged into the main branch. This includes the following functionality as explained above:
    - Party identification using SSIComms (Bloqzone solution)
    - Policy validation (Netis solution)
    

- The following third party components are used (further explained in the [architecture description](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Architecture.md)):
1.  [ACA-PY](https://github.com/hyperledger/aries-cloudagent-python): the agent that handles the communication between an INDY platform (eg Sovrin) and the Mandate application.
2.  [Tailserver](https://github.com/bcgov/indy-tails-server): the server that handles the distribution of the tails files to the INDY platform. Tails files are the basis for the revocation of credentials.
3.  Wallet: The ACA-PY needs to store information about created artifacts (eg credentials, schema's, connections). This is done in a wallet.


No specific mobile components are used. The solution can be used with Aries compatible wallets like Trinsic, Lissit, Esatus and the Animo based solution built by Bloqzone.

## Demo Application <a name="DemoApplication"></a>

Management interface of the application is available under https://ssimandate.vismaconnect.nl/ .

Be aware that the application is running in a demo environment. The application contains a lot of test schemas.

## Publications <a name="Publications"></a>
[Blog - authorizations-in-a-self-sovereign-world](https://info.vismaconnect.nl/blog/authorizations-in-a-self-sovereign-world)


## Final Words <a name="FinalWords"></a>
The project has been finished successfully. The SSI Mandate Service was extended with integrations with other tools/frameworks and it can be used by other parties, because of the documentation and the use of open source and open standards.

We are thankful to the eSSIF-Lab team for giving us the opportunity to do this project. We believe that the work that was made possible by the grant made the SSI Mandate Service richer in functionality and easier to use. We like to thank the eSSIF-Lab team for their support. The SSI Mandate Service will be developed and maintained further for specific cases and we will make sure that the service will be used by more parties.

In case of additional questions, feel free to contact: ralph.verhelst@visma.com.


