Bloqzone - Using SSIComms in mandate request flows
==================================================

# Table of contents
1. [Introduction](#Introduction)
2. [Solution](#Solution)
3. [Implementation](#Implementation)
    1. [Generating 2 SIP ID attributes](#GeneratingSIPIDs)
    2. [Filter SSIComms schemas](#FilterSSICommsSchemas)
    3. [Creating mandate: Filling in the SIP ID Attributes](#FillingInSIPIDs)
    4. [Creating mandate: Showing the Invitation Mandate URL](#ShowingInvitationMandateURL)


# Introduction <a name="Introduction"></a>
Bloqzone adds SSI to internet communications (SSIComms for short), so that identified communications are possible. SSIComms enables people to have any type of internet communication to exchange presentation requests and proofs and communicate at the same time.
So, when Alice calls Bob, Bob will be sure that it is really Alice he is talking to.
Visma Connect can add extra functionality by adding the ability to create and exchange mandates. Bob can create a mandate for Alice, so that Alice is authorized to access the data of Bob or is able do something on behalf of Bob.
In this case, Alice is the so called Authorized Representative and Bob is the Mandate Provider. 

# Solution <a name="Solution"></a>

Using SSIComms of Bloqzone, the identification of communicating parties is added to the mandate request flow of the SSI Mandate solution.

The mandate request flow will be as follows using SSIComms assuming Bob creates a mandate for Alice:
- Alice starts a SIP conversation (SIP:Invite) (or Bob initiates the SIP conversation).
- Bob sends a 200 OK
- Alice sends an acknowledgement
- Bob sends a text message containing SSI Out of Band message.
Alice does a DID Exchange over DIDComm.
- Bob verifies the identity of Alice.
- Bob creates a mandate proposal in the web application of the Mandate Service supplying the SIP identity numbers of Alice and himself.
- The Mandate Service sends the Mandate URL including the Mandate Proposal ID (MP-ID) back to Bob. / Bob sees the URL in the web and copies this to the app.
- Bob sends the mandate URL to Alice via the chat.
- Alice calls the Mandate URL which includes the Mandate proposal ID (or the wallet is opened first?)
- The Mandate Service sends a connection offer to Alice (after validating the mandate proposal id)
- Alice accepts the connection offer.
- The Mandate Service sends a Mandate Credential Offer to Alice
- Alice sends a Mandate Credential Request to the Mandate Service
- The Mandate Service issues the Mandate Credential to Alice

Remarks / questions:

- Step 1: Alice invites Bob, but Bob can initiate the SIP conversation as well.
- Bloqzone supports the wallet of Alice
- Aries / Indy protocol is covering the steps from Credential Offer (step 13) till Mandate Credential issuance (step 15)
- The creation of a Mandate Proposal has to be done after the DID Exchange, not before that exchange.
- After the creation of the Mandate Proposal, Bob verifies the identity of Alice.
- The mandate service returns the Invitation URL of the mandate credential (which includes MP-ID with the SIP-IDs of Bob and Alice), so that Bob can share that URL with Alice.


![Bloqzone SSI Mandate with Identification](Bloqzone_SSI_Mandate.png)


# Implementation <a name="Implementation"></a>
To support the mandate request flow for parties using SSIComms, the SSI Mandate Service has been extended:
1. Checking a "SSIComms" checkbox to generate SIP ID attributes: When creating a schema for a mandate, a checkbox can be set to generate 2 SIP ID attributes when the mandate has to be exchanged using SSIComms. The mandate provider has to fill in these 2 SIP ID attributes when creating a mandate.
2. Filtering "SSIComms" schemas: When selecting a schema for a mandate, Bob can set a checkbox to filter only the schemas that can be used together with SSIComms.
3. Filling in the SIP ID attributes: When creating the mandate, Bob can fill in the SIP ID attributes of himself and Alice.
4. Sharing the Mandate Invitation URL: After Bob has created the mandate, the web interface shows the invitation URL of the mandate credential with which Alice can retrieve the mandate. Bob will share this URL with Alice.
 

## Generating 2 SIP ID attributes <a name="GeneratingSIPIDs"></a> 
A "SSIComms" checkbox is introduced to generate 2 SIP ID fields when needed. This option is needed for being able to create a mandate schema with these two attributes. Based on that, a mandate proposal can be created by filling in those 2 SIP IDs. The mandate provider can supply the SIP IDs when creating the mandate proposal, directly after the SIP conversation has been established.


![Schema Manager generating 2 SIP Ids using SSIComms](Schema_Manager_SSIComms.png)

## Creating mandate: Filter SSIComms schemas <a name="FilterSSICommsSchemas"></a> 
A mandate will be created based on a schema. The option "Distributed through SSIComms" can be checked to filter the schemas that are compatible for SSIComms: 

![Mandate creator selecting a SSIComms compatible schema](Mandate_Creator_Select_Schemas.png)

In this case, the "Mandate Using SSIComms" schema is selected:

![Mandate creator selecting the schema](Mandate_Creator_Schema.png)


## Creating mandate: Filling in the SIP ID Attributes <a name="FillingInSIPIDs"></a> 
When the schema is selected, the mandate request screen is shown: 
![Mandate Request Screen](Mandate_Request_SSIComms_Filled.png)

The attributes based on the schema are shown: 
- the 2 SIP ID attributes 
- the attributes that are configured separately (in this case the attribute "amount" is added").


## Creating mandate: Showing the Invitation Mandate URL <a name="ShowingInvitationMandateURL"></a> 
The mandate create screen will generate and show the invitation URL to verifiable credential which includes the Mandate Proposal containing the SIP-IDs of Bob and Alice. Bob will share this URL with Alice, so that it can be called from the wallet of Alice.
This is what Bob sees after he has created the mandate:

![Screean with the Invitation URL ](Mandate_Creator_Invitation_URL.png)

When calling this invitation URL ( https://ssimandate.vismaconnect.nl/api/mandate/mandates/ba2c219f-9f15-4eaf-bcd4-abb6cb9409a3 ), it will return the following payload:

![Payload of the invitation](Payload_Invitation_URL.png)

With the JSON in the payload, the mandate credential for Alice can be retrieved.

