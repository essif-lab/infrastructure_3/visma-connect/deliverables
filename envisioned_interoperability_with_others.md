Envisioned interoperability with others
===========================

At this moment the Mandate Application is only connected to the Indy SSI Platform. 
The primary wallet is from Trinsic, but we envision to be interoperable with any SSI Platform. 
For this want to investigate the EASSI Gateway of TNO. Next to that, we want to make our solution more trustworthy by adding identification of parties and improve the robustness of our solution by supporting more complex mandate validations.

To achieve this, we want to cooperate with three parties:
1. Adding identification of the mandator and the mandatee (holder) using the SSIComms solution of Bloqzone
1. Integrating the EASSI gateway of TNO to improve interoperability by supporting more wallets.
1. Evaluating - together with Netis - how the mandate validations can be extended and how mandates can be made EBSI-compliant.
1. Researching what standards are available and what standards and agreements are needed to make the SSI Mandate service profitable and useful a an ecosystem.


## Bloqzone - SSIComms

_Solution: identifying the parties_

In the current situation, the mandate (or more formal: the request to accept the mandate) is sent to the mandate requester (holder) via a link in an email. The link consists of the invitation to make a connection with the mandate service and - as a second step - to accept the mandate credential.
In this case, the email address is the identifier of the mandate requester / holder.
The challenge is how you can prove who you are. How can you prove that you are the one who is entitled to that mandate.
In this mandate flow, the issuer (who issues the mandate, technically, the issuer is the mandate service) wants to verify who the holder (mandate requester) is. Bloqzone can offer a solution here. Parties can identify each other by a means of peer2peer connection using the SIP protocol. 
First, the issuer (mandator) will verify the holder before giving a mandate to that holder.
Bloqzone has built a ReactNative client containing the Animo SDK. In that client application, the link to the mandate credential will be placed (instead of an email with a link in the current situation).

How the identification of parties is added to the mandate workflow is explained in [Bloqzone-SSIComms](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Bloqzone%20-%20SSIComms.md).


## TNO - EASSI

_Solution: improving interoperability_

With the EASSI gateway we can - in addition to Trinsic - use other wallets such as Esatus. With incorporating EASSI, we improve interoperability as more wallets might be supported in the future. Unfortunately Animo that is used by Bloqzone is not supported yet. We can ask TNO to do that after all. We will configure the mappings to the different wallets. The mapping to the different wallet providers mainly determines which wallet we support (no mapping = no support). We will not support Irma, because it does not support revocations.
When using EASSI gateway, we will do interoperability testing. For every wallet that we support we will test whether it works with our SSI Mandate Service.

## Netis - Verifiable mandates

_Solution: adding complex mandate validations_

Netis offers a similar solution to ours. However, we can be more on the implementation side, Netis is more on the theoretical side (they have experts from the university). They have described the validation of mandates more thoroughly. They describe complex rules/validations and want to know if these can be implemented. Our current solution only validates mandates based on XML schema's (XSD) extended with value comparisons for specific attributes. Question is whether this should and/or could be extended.
Another thing is that Netis could indicate what the JSON context of a mandate credential must look like in order to make it EBSI compliant. So, more in general: what should an EBSI compliant mandate look like? Having and using these kind of standards makes our solution more interoperable.

How the validation of mandates is extended with declarative policies is explained in [Netis-Verifying Mandates](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Netis%20-%20Mandate%20Verification.md).


# Required standards and agreements
To make the SSI Mandate service profitable and useful in an ecosystem, we need to be able to base our solution on standards and agreements. Therefore, we will research - together with Netis - what standards are available and what standards and agreements are needed to use our solution in a sustainable ecosystem.

We will answer the questions:
- What standards are available? There are initiatives on data models and credentials. We will research which standards and agreements from W3C, DIF, eIDAS, EBSI (and even specific standards from for instance Aries [AIP](https://aries-interop.info/aries-interop-intro.html) )... are useful.
- What standards are useful for a SSI service like SSI Mandate and what will be beneficial of a commercial company like Visma? 
- What standards and agreements are lacking and need to be added to make a SSI Mandate service successful commercially after this ESSIF Lab project?

Next to the cooperations with parties in the ESSIF Lab (especially Netis), we are contributing to the Sovrin Guardianship Working Group (SGWG ) to talk about the use cases, standards and agreements regarding mandates and guardianships.



